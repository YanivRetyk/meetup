<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


    class User extends Authenticatable
{
    use Notifiable;
    
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }
    public function organization()
   {
       return $this->belongsTo('App\Organization', 'org_id');
   }
    public function meetingCreator()
    {
        return $this->hasMany('App\Meeting','inviter_id');
    }
    public function meetingInviteds()
    {
        return $this->belongsToMany('App\Meeting','invite');
    }
    public function topics()
    {
        return $this->hasMany('App\Topic');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role', 'organization','org_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}



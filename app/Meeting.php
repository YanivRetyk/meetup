<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable =[
        'title','start_meet','meeting_end' ];

        public function user(){
            return $this->hasMany('App\User');
        }
    
        public function meeting(){
            return $this->hasMany('App\Meeting');
        }
        public function organization(){
            return $this->hasMany('App\Organization');
        }
        
   
}

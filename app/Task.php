<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable =[
        'title','meeting_start','meeting_end' 
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function meeting(){
        return $this->belongsTo('App\Meeting');
    }
    public function user_id()
    {
        return $this->belongsTo('App\User');
    }

}
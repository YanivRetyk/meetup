<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Organization extends Model
{
    protected $table='organizations';
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    protected $fillable = [
        'org_name', 'min_topics'
    ];
    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }
  
}
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\Profile as Authenticatable;


    class Profile extends Authenticatable
{
    use Notifiable;
    
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }
    public function organization()
   {
       return $this->belongsTo('App\Organization', 'org_id');
   
    }
    public function meetingInviteds()
    {
        return $this->belongsToMany('App\Meeting','invite');
    }
    public function topics()
    {
        return $this->hasMany('App\Topic');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'remember_token',
    ];



}


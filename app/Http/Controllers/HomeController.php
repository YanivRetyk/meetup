<?php


namespace App\Http\Controllers;

use DB;
use App\User;
use App\Task;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Meeting;
use App\Topic;
use App\Invite;
use App\Organization;
use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\collection;//sorting, average, etc.
use Illuminate\Support\Facades\Validator;//verification


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
    $testing = DB::table('tasks')
        ->where('user_id', '=', Auth()->user()->id)
        ->where('status', '=', 1)
        ->get();


    $tas = DB::table('tasks')->where('user_id', '=', Auth()->user()->id)->get();

    $late = DB::table('tasks')
        ->where('user_id', '=', Auth()->user()->id)
        ->where('task_end', '<', NOW())
        ->where('status', '=', 0)
        ->get();   
    
        if($tas->count()!=0)
    $pre = (($tas->count()-$late-> count())/ $tas->count());
    else($pre=0);


    $datas =  DB::table('invites')
    ->join('meetings','meetings.id','=','invites.meeting_id')
        ->select('invites.user_id','meetings.title','meetings.meeting_start','meetings.org_id')
        ->where('invites.user_id', '=', Auth()->user()->id)
        ->where('org_id', '=', Auth()->user()->org_id)
        ->where('meeting_start', '>', NOW()->timezone('Asia/Jerusalem'))
       ->get();

    $months = array(1 => 'Jan.', 2 => 'Feb.', 3 => 'Mar.', 4 => 'Apr.', 5 => 'May', 6 => 'Jun.', 7 => 'Jul.', 8 => 'Aug.', 9 => 'Sep.', 10 => 'Oct.', 11 => 'Nov.', 12 => 'Dec.');
    $transposed = array_slice($months, date('n'), 12, true) + array_slice($months, 0, date('n'), true);
    $last6 = array_reverse(array_slice($transposed, -6, 12, true), true);

    $hmm = DB::table('meetings')->where('org_id', '=', Auth()->user()->org_id)->get();

    $hmp = DB::table('invites')->where('user_id', '=', Auth()->user()->id)->get();
    if ($datas->count()==0) { 
        $dday= "no meetings for you";
        $sub ="";
    }
    else{
    $dday=$datas->first()->meeting_start;
    $sub =$datas->first()->title;
    $count=0;
    foreach ($datas as $p) {

        if( $p->meeting_start > NOW()->timezone('Asia/Jerusalem') && $count==0){
            $dday=$p->meeting_start;
            $sub =$p->title;
            $count=1;
        }
        if( $p->meeting_start > NOW()->timezone('Asia/Jerusalem') && $p->meeting_start < $dday){
        $dday=$p->meeting_start;
        $sub =$p->title;
    }

    }
    }

        return view('dashboard',compact('testing','tas', 'late', 'pre', 'last6', 'hmm', 'datas', 'hmp', 'dday','sub'));
    }
}

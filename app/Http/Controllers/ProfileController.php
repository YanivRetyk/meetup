<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use DB;
use App\User;
use App\Topic;
use App\Invite;
use App\Task;
use App\Organization;
use Carbon\Carbon;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\collection;//sorting, average, etc.
use Illuminate\Support\Facades\Validator;//verification
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        $org = Organization::All();
        return view('organizations.edit', compact('org','user'));
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request, $id)
    {

$affected = DB::table('organizations')
            ->where('organization_id', $id)
            ->update(['min_topic' => $request->min_topic]);
            


/*
        $user=User::findOrFail($id)->get();
        $ird= $user->org_id->get();
        $org=Organization::findOrFail($ird);

        $org->update($request->except(['_token']));
        */
        return redirect(route('users.index'));
        

    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }
}

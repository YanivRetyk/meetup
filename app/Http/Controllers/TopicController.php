<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\Task;
use App\Topic;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('topics.index')->with ('topics',Topic::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $meeting_id = $request->get('meeting_id');
         return view('topics.create')->with('meeting_id', $meeting_id);
    }

     public function moveTo($subject_id)
    {
        $subject = Topic::where('id',$subject_id)->first();
        $meeting_id=$subject->meeting_id;
        $meeting=Meeting::where('id',$meeting_id)->first();
        $organization_id= $meeting->org_id;
        return view('meetings.moveTo')->with ('meetings',Meeting::where('org_id',$organization_id)->get())->with('subject_id',$subject_id);
    }

    public function moveToThisMeeting($meeting_id,$subject_id)
    {
       $id_subject=$subject_id;
       $id_meeting= $meeting_id;
       $subject = Topic::where('id',$id_subject)->firstOrFail();
       $subject->meeting_id=$id_meeting;
       $subject->save();    
       return redirect(route('meetings.index')); 

      // return 'the subject has been move';
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($meeting_id,Request $request)
    {
        $topic=new Topic();
        $id = Auth::id();
        $topic->meeting_id= $meeting_id;
        $topic->time_start=$request->topic_start;
        $topic->time_end=$request->topic_end;
        $topic->subject=$request->subject;
        $topic->status=0;
        $topic->save();
       
        $topics = Topic::where('meeting_id', $meeting_id)->get();
        $tasks = Task::where('meeting_id', $meeting_id)->get();
        return view('tasks.meetingTasks',compact('tasks','topics','meeting_id'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function donet($id)
    { 
        
        $topic = Topic::findOrFail($id);            
        $topic->status = 1; 
        $topic->save();
        $back =$topic->meeting_id;

        return redirect(route('meetingTasks',$back));    
    }  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        return view('topics.create')->with('topic', $topic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopicsRequest $request,Topic $topic)
    {
        $topic->save();
        return redirect(route('topics.index')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        $topic->delete();
        return redirect(route('meeting.index'))->withStatus(__('Topic successfully deleted.'));
    }
}

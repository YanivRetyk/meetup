<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Task;
use App\Meeting;
use App\Topic;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use App\Http\Requests\Tasks\CreatTaskRequests;
use App\Http\Requests\Tasks\UpdateTasksRequest;



class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::where('user_id',Auth::id())->get();
        return view('tasks.index')->with ('tasks',Task::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function meetingTasks($meeting_id)
    {
       $time =  NOW()->timezone('Asia/Jerusalem');
       $id=$meeting_id;
       $meeting = Meeting::findOrFail($id); 
       $topics = Topic::where('meeting_id', $id)->get();
       $tasks = Task::where('meeting_id', $id)->get();
       $datas =  DB::table('users')
       ->join('tasks','tasks.user_id','=','users.id')
       ->select('users.id','users.name' , 'tasks.title', 'tasks.task_end', 'tasks.status', 'tasks.user_id', 'tasks.id as qs', 'tasks.meeting_id', 'tasks.creator_id' )
      ->get();
       return view('tasks.meetingTasks',compact('tasks', 'topics', 'meeting_id','datas', 'meeting', 'time'));
    } 
    
    public function done($id)
    { 
        
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('tasks');    
    }   
    

    public function create(Request $request)
    {
        $meeting_id =  $request->get('meeting_id');
        return view('tasks.create' , [ 'users'=>User::all()])->with('meeting_id', $meeting_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($meeting_id, Request $request)
    {
        $task=new Task();
        $id = Auth::id();
        $task->title=$request->title;
        $task->user_id=$request->user_id;
        $task->meeting_id= $meeting_id;
        $task->task_start=$request->task_start;
        $task->task_end=$request->task_end;
        $task->creator_id=$id;
        $task->status=0;
        $task->save();
        $time =   NOW()->timezone('Asia/Jerusalem');
        $id=$meeting_id;
        $meeting = Meeting::findOrFail($id); 
        $topics = Topic::where('meeting_id', $id)->get();
        $tasks = Task::where('meeting_id', $id)->get();
        $datas =  DB::table('users')
        ->join('tasks','tasks.user_id','=','users.id')
        ->select('users.id','users.name' , 'tasks.title', 'tasks.task_end', 'tasks.status', 'tasks.user_id', 'tasks.id as qs', 'tasks.meeting_id', 'tasks.creator_id' )
       ->get();
        return view('tasks.meetingTasks',compact('tasks', 'topics', 'meeting_id','datas', 'meeting', 'time'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('tasks.create')->with('task', $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTasksRequest $request,Task $task)
    {
        $task->save();
        return redirect(route('tasks.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task, $meeting_id)
    {
        $task->delete();
        return redirect(route('meetingTasks, $meeting_id'))->withStatus(__('Task successfully deleted.'));
       
    }
}

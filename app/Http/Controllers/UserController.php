<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use App\Task;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Meeting;
use App\Topic;
use App\Invite;
use App\Organization;
use Carbon\Carbon;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\collection;//sorting, average, etc.
use Illuminate\Support\Facades\Validator;//verification

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $time =   NOW()->timezone('Asia/Jerusalem');
        $datas =  DB::table('users')
        ->join('tasks','tasks.user_id','=','users.id')
        ->select('tasks.task_end', 'tasks.status', 'tasks.user_id','users.id' )
       ->get();
        return view('users.index',['users' => $model->paginate(15)],compact('datas', 'time'));
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $model)
    {
        $model->create($request->merge(['password' => Hash::make($request->get('password'))])->all());

        return redirect()->route('user.index')->withStatus(__('User successfully created.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,  $id)
    {
        $user=User::findOrFail($id);

        $user->update($request->except(['_token']));
        return redirect(route('users.index'));
        
       
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        $user->delete();

        return redirect()->route('user.index')->withStatus(__('User successfully deleted.'));
    }

    public function updateOrg(Request $request)
    {

             
            $this->validate(
                $request,
                [
                    'min_topic' => 'required',
                ]
            );

            $affected = DB::table('organizations')
            ->where('organization_id', Auth()->user()->org_id)
            ->update(['min_topic' => $request->min_topic]);

            return redirect()->route('user.index');

    }
    public function editOrg()
    {
        $organization = DB::table('organizations')
        ->where('organization_id', '=', Auth()->user()->org_id)->get();
            $id = Auth::user()->org_id;

            return view('users.editMin');
      
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Meeting;
use App\User;
use App\Topic;
use App\Invite;
use App\Task;
use App\Organization;
use Carbon\Carbon;
use App\Http\Requests\Meetings\CreatMeetingRequests;
use App\Http\Requests\Meetings\UpdateMeetingsRequest;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\collection;//sorting, average, etc.
use Illuminate\Support\Facades\Validator;//verification
use Illuminate\Support\Facades\Hash;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function index()
    {
        $meetings =Meeting::all();
        $datas =  DB::table('invites')
        ->join('meetings','meetings.id','=','invites.meeting_id')
        ->select('invites.meeting_id','invites.user_id' , 'meetings.id', 'meetings.inviter_id','meetings.title','meetings.org_id','meetings.status','meetings.meeting_start','meetings.meeting_end' )
       ->get();
        return view('meetings.index',compact('datas','meetings'));
    
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $org_id = Auth::user()->org_id;
            $orgs = Organization::where('organization_id', $org_id)->get();
            $organization=Auth::user()->org_id;
            $users=User::all();
            $topics = Topic::all();

            return view ('meetings.create', compact('orgs',
                'organization',
                'users', 'topics')

            
    );
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $meeting=new Meeting();
        $id = Auth::id();
        $meeting->inviter_id=$id;
        $meeting->title=$request->title;
        $meeting->meeting_start=$request->meeting_start;
        $meeting->meeting_end=$request->meeting_end;
        $meeting->org_id= Auth::user()->org_id;
        $meeting->status=0;
        $meeting->save();

        foreach($request->inviteds as $invited){
            Invite::create([
                'meeting_id'=>$meeting->id,
                'user_id'=>$invited,
            ]);
        }


        foreach ($request->myInputs as $eachInput) {
            Topic::create([
                'meeting_id'=>$meeting->id,
                'subject'=>$eachInput,
                'status'=>0,

        ]);
}


        return redirect(route('meetings.index'));
        


                 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        return view('meetings.create')->with('meeting', $meeting);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateMeetingsRequest $request, Meeting $meeting)
    {
        $meeting->title=$request->title;
        $meeting->save();
        return redirect(route('meetings.index')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($meeting)
    {
        $meeting->delete();
        return redirect(route('meetings.index'))->withStatus(__('Meeting successfully deleted.'));
    }
}

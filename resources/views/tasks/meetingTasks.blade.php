
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
 @include('users.partials.header', [
            'title' => __('Hello') . ' '. auth()->user()->name,
            'description' => __('On this page you can view allsubjects and tasks of meeting'),
            'class' => 'col-lg-7'
        ]) 

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                {{--<div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                            </div>
                            <div class="col-4 text-right">
                            
                            @if(Auth::user()->role != 'participant')
                            <a href="{{route('topics.create',['meeting_id'=>$meeting_id])}}"  class="btn btn-sm btn-primary">{{ __('add a topic') }}</a>
                           @endif
                            </div>
                        </div>
                    </div> --}}
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('subject') }}</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($topics as $topic)
                                    <tr>
                                        <td>{{$topic->subject}}</a></td>
                                    @if(Auth::user()->role != 'participant')   
                                        <td>  
                                         @if($meeting->meeting_start < $time  && $meeting->meeting_end > $time)
                                            @if ($topic->status == 0)
                                            <a href="{{route('donet', $topic->id)}}">Mark As done</a>
                                                @else
                                                {{ __('enough with this topic') }}
                                            @endif
                                            @else
                                            <td></td>
                                        @endif</td>
                                        <td></td>
                                        <td><a href ="{{route('moveTo',$topic->id)}}" class="btn btn-info btn-sm">move to other meeting</a></td>
                                        <td>
                                            <form method="post" action ="{{action('TopicController@destroy',$topic->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this topic?") }}') ? this.parentElement.submit() : ''">
                                                    <input type ="submit" class=" btn btn-danger btn-sm" name="submit" value ="Delete"> 
                                                </button>
                                            </form>
                                        </td>
                                    @endif
                                    </tr>
                                    
                                @endforeach
                            </tbody>
                        </table>


    

        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                            </div>
                            <div class="col-4 text-right">
                            
                            @if(Auth::user()->role != 'participant')
                            <a href="{{route('tasks.create',['meeting_id'=>$meeting_id])}}"  class="btn btn-sm btn-primary">{{ __('add a task') }}</a>
                            @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                        <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Task Title') }}</th>
                                    <th scope="col">{{ __('deadline') }}</th>
                                    <th scope="col">{{ __('status') }}</th>
                                     <th scope="col">{{ __('assigned to') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $datas as $data )
                                    @if($data->creator_id == $data->id || $data->user_id == $data->id)
                                    @if($data->meeting_id == $meeting_id)
                                    <tr>
                                        <td>{{$data->title}}</a></td>
                                        @if($data->task_end != null)
                                        <td>{{$data->task_end}}</td>
                                        @else()
                                        <td></td>
                                        @endif
                                        @if($data->status == 0)
                                        <td>{{'wating to be done'}}</td>
                                        @else()
                                        <td>{{ __('done') }}</td>
                                        @endif
                                        <td>{{$data->name}}</td>
                                        <td>
                                             
                                        <td class="text-right">
                                       
                                      
                                   </td>
                                  
                                    </tr>
                                    @endif
                                    @endif
                                @endforeach
                            </tbody>
                        </table>

                        @endsection                   
@section('scripts')
    <script>
    function handleDelete(id){
        var form = document.getElementById("deleteTaskForm");
        form.action= 'tasks/'+ id; 
        console.log('deleting.',form);
        $('#deleteModal').modal('show')
    }
    
    function handleDelete(id){
        var form = document.getElementById("deleteTopicForm");
        form.action= 'topics/'+ id; 
        console.log('deleting.',form);
        $('#deleteModal').modal('show')
    }
    </script>
@endsection
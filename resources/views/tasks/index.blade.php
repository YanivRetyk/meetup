
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
@include('users.partials.header', [
            'title' => __('Hello') . ' '. auth()->user()->name,
            'description' => __('On this page you can see all the tasks you are required to do and also the tasks you have assigned to other users'),
            'class' => 'col-lg-7'
        ]) 
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="text-overflow m-0">{{ __('My tasks') }}</h3>
                            <div class="row align-items-center">
                                <div class="col-8">
                                </div>
                                <div class="col-4 text-right">
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                   
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Title') }}</th>
                                     <th scope="col">{{ __('deadline') }}</th>
                                     <th scope="col">{{ __('mark as done') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $task)
                                    <tr>
                                @if($task->user_id==Auth::user()->id)
                                        <td>{{$task->title}}</td>
                                        <td>{{ $task->task_end }}</td>
                                        <td>  
                                            @if ($task->status == 0)
                                            <a href="{{route('done', $task->id)}}">Mark As done</a>
                                                @else
                                                    Done!
                                            @endif</td>

                                       
                                        @endif
                                    </tr>
                               
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
                <div class="col-12">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>
                <br>
                <br>
                @if (Auth::user()->role != 'participant')
                <div class="row">
                    <div class="col">
                        <div class="card shadow">
                            <div class="card-header border-0">
                            <h3 class="text-overflow m-0">{{ __('Tasks I have assgined') }}</h3>
                            <br>

                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">{{ __('Title') }}</th>
                                                {{--<th scope="col">{{ __('Assigned to') }}</th>--}}
                                                <th scope="col">{{ __('Deadline') }}</th>
                                                <th scope="col">{{ __('Is it done?') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($tasks as $task)
                                                <tr>
                                            @if($task->creator_id==Auth::user()->id)
                                                    <td>{{$task->title}}</td>
                                                        {{--@foreach ($users as $user)
                                                            $nameId=$task->user_id==$user->id
                                                            $name=User:where('name',$nameId)->firstOfAll();
                                                            <td>$name</td>
                                                            @endforeach
                                                        <td>{{$task->user_id}}</td>--}}
                                                    <td>{{ $task->task_end }}</td>
                                                    <td>  
                                                        @if ($task->status == 0)
                                                        {{ __('not yet') }}
                                                            @else
                                                                Done
                                                        @endif</td>

                                                    @endif
                                                </tr>
                                            
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div>
                    @endif
                </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                        </nav>
                    </div>
            </div>
        
        @include('layouts.footers.auth')
        </div>
    </div>
</div>
@endsection

@section('scripts')
 <script>
  function handleDelete(id){
      var form = document.getElementById("deleteTaskForm");
      form.action= 'tasks/'+ id; 
      $('#deleteModal').modal('show')
  }
 </script>
@endsection


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Add Task')])   

   
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('tasks.index') }}" class="btn btn-sm btn-primary">{{ __('Show Tasks') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('store',[$meeting_id])}}" autocomplete="off">
                            @csrf
                            {{csrf_field()}}
                          <script src="/assets/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

                            <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-title">{{ __('Title') }}</label>
                                    <input type="text" name="title" id="input-title" value=""
                                     class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Title') }}" value="{{ old('title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                    <label class="form-control-label" for="task_end">{{ __('deadline') }}</label>
                                <div class="pl-lg-4">
                                    <div class="form-group{{ $errors->has('task_end') ? ' has-danger' : '' }}">
                                    <input class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                    value="" name="task_end" id="task_end" type="datetime-local" placeholder="{{ __('due date') }}"  />
                                    @if ($errors->has('task_end'))
                                        <span id="date-error" class="error text-danger" for="task_end">
                                        <strong>{{ $errors->first('task_end') }}</strong>
                                        </span>
                                    @endif
                                    </div>

                                    <label class="form-control-label" for="user_id">{{ __('asign task to:') }}</label>
                                    <select class="form-control" name="user_id">
                                @foreach($users as $user)
                                        @if($user->org_id==Auth::user()->org_id )                            
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endif
                                        @endforeach
                                            </select>   


                                </div>
                                </div>

                               
                                </div>
                                </div>
                                    <button type="submit" class="btn btn-success mt-4">
                                    {{ 'Create'}}</button>
                                </div>
                            </div>
                        </form>
                                
                </div>
            
        
        @include('layouts.footers.auth')
    </div>
@endsection


<script src="/assets/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

@section('js')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
@endsection


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                            </div>
                            <div class="col-4 text-right">
                            @if(Auth::user()->role == 'admin')
                                <a href="{{route('topics.create')}}"  class="btn btn-sm btn-primary">{{ __('add a topik') }}</a>
                            @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Subject') }}</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                     <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($topics as $topic)
                                @if($topic->org_id==Auth::user()->org_id)
                                    <tr>
                                        <td><a href ="">{{$topic->subject}}</a></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                             <form action="{{ route('topic.destroy', $topic) }}" method="post">
                                                            @csrf
                                                            @method('delete')
                                                            
                                                            <a class="dropdown-item" href="{{route('',$topic->id)}}">{{ __('Edit') }}</a>
                                                        
                                                            <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? handleDelete({{$topic->id}}) : ''">
                                                                {{ __('Delete') }}
                                                            </button>
                                             </form>  
                                        </td>

                                       
                                       @if(Auth::user()->role != 'participant')
                                        <td class="text-right">
                                       
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                  
                                                        <form action="{{ route('topic.destroy', $topic) }}" method="post">
                                                           
                                                            <a class="dropdown-item" href=" {{isset($topic)?'Update topic' : 'Edit New topic'}}}">{{ __('Edit') }}</a>
                                                    
                                                        </form>    
                                                    <!--@else-->
                                                        <a class="dropdown-item" href="{{ route('organization.edit') }}">{{ __('Edit') }}</a>
                                                   
                                                    
                                                </div>
                                            </div>
                                        </td>
                                        @endif
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection

@section('scripts')
 <script>
  function handleDelete(id){
      var form = document.getElementById("deleteTopicForm");
      form.action= 'topics/'+ id; 
      $('#deleteModal').modal('show')
  }
 </script>
@endsection

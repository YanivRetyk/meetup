
@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Add User')])   

   
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                               <!-- <h3 class="mb-0"> {{isset($topic)? "Edit Task Category" :"Create Task Category" }}</h3>-->
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('topics.index') }}" class="btn btn-sm btn-primary">{{ __('Show topics') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{isset($topic)?route('topics.update',$topic->id) : route('topics-store',[$meeting_id])}}" autocomplete="off">
                            @csrf
                            {{csrf_field()}}
                            @if(isset($topic))
                            @method('PUT')
                            @endif 
                            <script src="/assets/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

                            <h6 class="heading-small text-muted mb-4">{{ __('Topic') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('subject') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-subject">{{ __('Subject') }}</label>
                                    <input type="text" name="subject" id="input-subject" value="{{isset($topic) ? $topic->subject :''}}"
                                     class="form-control form-control-alternative{{ $errors->has('subject') ? ' is-invalid' : '' }}" placeholder="{{ __('Subject') }}" value="{{ old('subject') }}" required autofocus>

                                    @if ($errors->has('subject'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('subject') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                </div>


                                </div>
                                </div>
                                    <button type="submit" class="btn btn-success mt-4"> {{isset($topic)?'Update Topic' : 'Add'}}</button>
                                </div>
                            </div>
                        </form>
                                
                </div>
            
        
        @include('layouts.footers.auth')
    </div>
@endsection


<script src="/assets/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

@section('js')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
@endsection

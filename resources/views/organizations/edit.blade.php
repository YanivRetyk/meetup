@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('On this page, you can change the minimum number of topics that must be set when scheduling a meeting'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Edit the minimum number of topics for each meeting') }}</h3>
                        </div>
                    </div>
                    @foreach ($$org as $$orgs)
                    @if($org->organiation_id == $user->id) 
                    <div class="card-body">
                        <form method="post" action="{{ route('organization.update', $org) }}" autocomplete="off">
                            @csrf
                            @method('put')

                     


                            
                            <div class="form-group">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"></span>
                                    </div>
                                    <label class="form-control-label" for="input-min_topic">{{ __('min_topic') }}</label>
                                <label class="form-control-label" for="min_topic"></label>
                                    <input name="min_topic"  id="input-min_topic" class="form-control form-control-alternative{{ $errors->has('min_topic') ? ' is-invalid' : '' }}" placeholder="{{ __('min_topic') }}" value="{{ old('min_topic', $org->min_topic) }}"  required>
                               
                                    @if ($errors->has('min_topic'))
                                       <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('min_topic') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Sa ve') }}</button>
                                </div>
                            </div>
                        </form>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
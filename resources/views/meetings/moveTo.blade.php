@extends('layouts.app')
@section('content')



<div class ="card card -default">
    <div class="card-header">meeting</div>
     <div class = "card-body">
      <table class="table">
       <thead>
        <th>to which meeting do you want to move the subject?</th>
       
        <th></th>
        
       </thead>
       <tbody>
        @foreach($meetings as $meeting)
         <tr>
         <td><a href ="{{route('moveToThisMeeting',[$meeting->id,$subject_id])}}">{{$meeting->title}}</a></td>
        @endforeach 
       </tbody>
      </table>
      
@endsection

@section('scripts')
 <script>
  function handleDelete(id){
      var form = document.getElementById("deleteMeetingForm");
      form.action= 'meetings/'+ id; 
      $('#deleteModal').modal('show')
  }
 </script>
@endsection
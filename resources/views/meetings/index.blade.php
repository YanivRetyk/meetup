
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', [
            'title' => __('Hello') . ' '. auth()->user()->name,
            'description' => __('On this page you can view all meetings you are invited to'),
            'class' => 'col-lg-7'
        ]) 

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Meetings') }}</h3>
                        </div>
                            <div class="row align-items-center">
                                <div class="col-8">
                                </div>
                            <div class="col-4 text-right">
                                @if(Auth::user()->role != 'participant')
                                    <a href="{{route('meetings.create')}}"  class="btn btn-sm btn-primary">{{ __('Create a new meeting') }}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                    @if(Auth::user()->role != 'participant')
                    <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Meetings I Created') }}</h3>
                        </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Title') }}</th>
                                    {{--<th scope="col">{{ __('How many particepants') }}</th>--}}
                                    <th scope="col"></th>
                                     <th scope="col">{{ __('Time') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($meetings as $meeting)
                                    <tr>
                                    @if(Auth::user()->role != 'participant' && $meeting->inviter_id==Auth::user()->id)  
                                            <td><a href ="{{route('meetingTasks',$meeting->id)}}">{{$meeting->title}}</a></td>
                                            {{--<td></td>--}}
                                            <td></td>
                                            <td>{{ $meeting->meeting_start }}</td>
                                            <td></td>
                                    @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                    <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Meetings Im invited to') }}</h3>
                        </div>
                        @endif
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Title') }}</th>
                                    {{--<th scope="col">{{ __('How many particepants') }}</th>--}}
                                    <th scope="col"></th>
                                     <th scope="col">{{ __('Time') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                    <tr> 
                                        @if($data->user_id==Auth::user()->id)
                                            <td><a href ="{{route('meetingTasks',$data->id)}}">{{$data->title}}</a></td>
                                            {{--<td></td>--}}
                                            <td></td>
                                            <td>{{ $data->meeting_start }}</td>
                                            <td><a href="http://www.google.com/calendar/render? action=TEMPLATE&text={{$data->title}}&dates=/&details=[description]&location=[location]&trp=false &sprop= &sprop=name:" target="_blank" rel="nofollow">Add to my calendar</a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection

@section('scripts')
 <script>
  function handleDelete(id){
      var form = document.getElementById("deleteMeetingForm");
      form.action= 'meetings/'+ id; 
      $('#deleteModal').modal('show')
  }
 </script>
@endsection


@extends('layouts.app', ['title' => __('User Management')])
User
@section('content')
    @include('users.partials.header', ['title' => __('create a meeting')])   

    @foreach ($orgs as $org)
    @if($org->organization_id == Auth::user()->org_id)
    {{$min = $org->min_topic }}
    @endif
@endforeach

    <script type='text/javascript'>
      var min ="<?php echo $min; ?>";
      var counter = min;
var limit = min;

function addInput(divName){
    // if (counter > min)  {
     //     alert("yo need at leest " + min + " inputs");
    // }
    // else {
          var newdiv = document.createElement('div');
          newdiv.innerHTML = "topic "+ " <br><input type='text' class='form-control form-control-alternative{{ $errors->has('topic') ? ' is-invalid' : '' }}' name='myInputs[]'>";
          document.getElementById(divName).appendChild(newdiv);
          counter++;
    // }
}
</script>

<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

</script>
<script src="/wp-includes/js/addInput.js" language="Javascript" type="text/javascript"></script>


    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0"> {{isset($meeting)? "Edit Meeting Category" :"Create Meeting Category" }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('meetings.index') }}" class="btn btn-sm btn-primary">{{ __('Show Meetings') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{isset($meeting)?route('meetings.update',$meeting->id) : route('meetings.store')}}" autocomplete="off">
                            @csrf
                            {{csrf_field()}}
                            @if(isset($meeting))
                            @method('PUT')
                            @endif 
                            <script src="/assets/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-title">{{ __('Title') }}</label>
                                    <input type="text" name="title" id="input-title" value="{{isset($meeting) ? $meeting->title :''}}"
                                     class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Title') }}" value="{{ old('title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <label class="form-control-label" for="meeting_start">{{ __('Starting') }}</label>
                                <div class="pl-lg-4">
                                    <div class="form-group{{ $errors->has('meeting_start') ? ' has-danger' : '' }}">
                                    <input class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                    value="{{isset($meeting) ? $meeting->meeting_start :''}}" name="meeting_start" id="meeting_start" type="datetime-local" placeholder="{{ __('meeting start') }}"  required />
                                    @if ($errors->has('meeting_start'))
                                        <span id="date-error" class="error text-danger" for="meeting_start">
                                        <strong>{{ $errors->first('meeting_start') }}</strong>
                                        </span>
                                    @endif
                                    </div>

                                    <label class="form-control-label" for="meeting_end">{{ __('Ending') }}</label>
                                <div class="pl-lg-4">
                                    <div class="form-group{{ $errors->has('meeting_end') ? ' has-danger' : '' }}">
                                    <input class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                    value="{{isset($meeting) ? $meeting->meeting_end :''}}" name="meeting_end" id="meeting_end" type="datetime-local" placeholder="{{ __('meeting end') }}"  required />
                                    @if ($errors->has('meeting_end'))
                                        <span id="date-error" class="error text-danger" for="meeting_end">
                                        <strong>{{ $errors->first('meeting_end') }}</strong>
                                        </span>
                                    @endif
                                    </div>


                                     <label class="form-control-label" for="meeting_end">{{ __('Hold down the Ctrl button and pic participents for your meeting') }}</label>
                                    <select  class="form-control" data-role="tagsinput"  name="inviteds[]" multiple="multiple">
                                            <option value="{{Auth::user()->id}}" selected>me</option>
                                    
                                    @foreach($users as $user)
                                            @if($user->org_id==Auth::user()->org_id&& $user->id!=Auth::user()->id)                            
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endif
                                     @endforeach
                                    </select>

<br>
<br>                                
<br>                               
                                    
                                <label class="form-control-label" for="meeting_start">{{ __('Meeting topics- you must have at least '.$min.' topics') }}</label>
                                @for($i=1;$i<=$org->min_topic;$i++)
                                     <div class="pl-lg-4">
                                         <div id="dynamicInput">
                                         {{ __('Topic ') }}<br><input class="form-control form-control-alternative{{ $errors->has('topic') ? ' is-invalid' : '' }}"type="text" name="myInputs[]" required/>
                                         </div>
                                    </div>
                                @endfor
                                <input type="button" class="btn btn-sm btn-primary" value="Add another topic" onClick="addInput('dynamicInput');">


                                </div>
                                </div>

                               
                                </div>
                                </div>
                                    <button type="submit" class="btn btn-success mt-4">
                                    {{'Set New Meeting'}}</button>
                                </div>
                            </div>
                        </form>
                                
                </div>
            
        
        @include('layouts.footers.auth')
    </div>
@endsection


@section('script')
<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

</script>


@endsection

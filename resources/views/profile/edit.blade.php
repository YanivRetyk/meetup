@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('On this page, you can change the minimum number of topics that must be set when scheduling a meeting'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Edit the minimum number of topics for each meeting') }}</h3>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="post" action="{{ route('profile.update', $user->org_id) }}" autocomplete="off">
                            @csrf
                            @method('put')

                         

                            
                            <div class="form-group">
                                <div class="input-group input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"></span>
                                    </div>
                                    <input class="form-control" placeholder="{{ __('min num of topics') }}" type="int" name="min_topic" value="{{ old('org_id') }}" required>
                                </div>
                                @if ($errors->has('org_id'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('org_id') }}</strong>
                                    </span>
                                @endif
                            </div>


                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
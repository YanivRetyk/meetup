<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">how many tasks i have</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$tas->count()}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">how many tasks i deliverd</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$testing->count()}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                        <i class="fas fa-chart-pie"></i>
                                    </div>
                                </div>
                            </div>
                       
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0 bg-warning">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-white mb-0">how many tasks I'M LATE on</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$late->count()}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$pre*100}}%</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                        <i class="fas fa-percent"></i>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>                        <h3 class="col-12 mb-0 text-white">{{ __('Meetings') }}</h3>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">how many meeting in my org</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$hmm->count()}}</span>
                                </div>
                                <div class="col-auto">
                                <div class="icon icon-shape bg-gray 600 text-white rounded-circle shadow">
                                <i class="ni ni-air-baloon"></i>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">how many meeting i'm invited to</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$hmp->count()}}</span>
                                </div>
                                <div class="col-auto">
                                <div class="icon icon-shape bg-default text-white rounded-circle shadow">
                                <i class="ni ni-circle-08"></i>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0 bg-info">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-white mb-0">My next meeting:</h5>
                                    <span class="h2 font-weight-bold mb-0">@if($datas->count()==0)
                                    {{'no meetings for you'}}
                                     @else {{$dday}}<br><h5 class="card-title text-uppercase text-white mb-0">meeting:</h5>{{$sub}}
                                     @endif</span>
                                </div>
                                <div class="col-auto">
                                <div class="icon icon-shape bg-white text-gray 900	 rounded-circle shadow">
                                <i class="ni ni-time-alarm "></i>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} <a >Amit Gantz, Yarden Erera, Yuval Alster, Yaniv Retyk</a>
</div></div></div>
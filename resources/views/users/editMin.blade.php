@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('On this page, you can change the minimum number of topics that must be set when scheduling a meeting'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Edit the minimum number of topics for each meeting') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                    <form method="post" action= "{{ route('updateOrg' )}}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

                     


                            
                            <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Minimum number of topics') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('min_topic') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('min_topic') ? ' is-invalid' : '' }}" name="min_topic" id="input-min_topic" type="text" placeholder="{{ __('Minimum topics') }}" value="what is the minimum num of topics?" required />
                      @if ($errors->has('min_topic'))
                        <span id="min_topic-error" class="error text-danger" for="input-min_topic">{{ $errors->first('min_topic') }}</span>
                      @endif
                    </div>
                  </div>
                  </div>


                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('users.partials.header', [
            'title' => __('Hello') . ' '. auth()->user()->name,
            'description' => __('On this page you can change the role of any member of your organization'),
            'class' => 'col-lg-7'
        ])       

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Role') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body"> 
                    <form method="post" action="{{ route('user.update', $user) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

                          
                                <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('role') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-role">{{ __('role') }}</label>
                                <label class="form-control-label" for="role"></label>
                                    <select name="role"  id="input-role" class="form-control form-control-alternative{{ $errors->has('role') ? ' is-invalid' : '' }}" placeholder="{{ __('role') }}" value="{{ old('role', $user->role) }}" required autofocus >
                                            <option value="" selected disabled>Current Role: {{ $user->role }}</option>
                                            <option value='participant'>{{ __('Participant') }}</option>
                                            <option value='admin'>{{ __('admin') }}</option>
                                            <option value='meetingorganizer'>{{ __('Meeting Organizer') }}</option>
                                    </select>
                                    @if ($errors->has('role'))
                                       <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif
                                </div>


                               <div class="text-center">
                                   <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
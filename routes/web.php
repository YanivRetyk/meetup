<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('meetingtasks/donet/{id}', 'TopicController@donet')->name('donet');
Route::get('tasks/done/{id}', 'TaskController@done')->name('done');
Route::resource('/meetings','MeetingController')->middleware('auth');
Route::resource('meeting', 'MeetingController');
Route::post('meeting/store', 'MeetingController@store')->name('store');
Route::get('meeting/create', 'MeetingController@create')->name('create');
Route::resource('/users','UserController')->middleware('auth');
Route::resource('users', 'UserController');
Route::resource('/organizations','OrganizationController')->middleware('auth');
Route::resource('organizations', 'OrganizationController');
Route::resource('/profile','ProfileController')->middleware('auth');
Route::resource('profile', 'ProfileController');
Route::put('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@update']);
Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
Route::put('organizations', ['as' => 'organizations.edit', 'uses' => 'OrganizationController@update']);
Route::put('organizations', ['as' => 'organizations.update', 'uses' => 'OrganizationController@update']);


Route::get('tasks/meetingTasks/{meeting_id}','TaskController@meetingTasks')->name('meetingTasks');
Route::get('/neworg', function () { return view('neworg');})->name('neworg');
Route::resource('meeting', 'MeetingController');
Route::resource('organization', 'ProfileController');
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::put('organization', ['as' => 'organizations.edit', 'uses' => 'OrganizationController@update']);
	Route::put('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@update']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('user', ['as' => 'users.edit', 'uses' => 'UserController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::get('editmin', 'UserController@editOrg')->name('editMin');
	Route::put('editMin', 'UserController@updateOrg')->name('updateOrg');
});

Route::post('/tasks/{meeting_id?}','TaskController@store')->name('store');
Route::post('/topics-store/{meeting_id?}','TopicController@store')->name('topics-store');

Route::resource('/meetings','MeetingController')->middleware('auth');
Route::resource('/tasks','TaskController')->except(['store']);
Route::resource('/topics','TopicController')->except(['store']);
Route::get('tasks/{meeting_id}','TaskController@store');
Route::get('topics/{meeting_id}','TopicController@store');

//didint go over yet
Route::group(['middleware' => 'auth'], function () {
	//Route::get('table-list', 'MeetingController@index')->name('table');
	Route::get('/dashboard', 'MeetingController@dashboard')->name('dashboard');
	Route::get('passed_meetings', 'MeetingController@passed')->name('passed_meetings');
	Route::get('notifications', 'MeetingController@tasks')->name('notifications');
	Route::get('/meetings/{id}','MeetingController@show')->name('meeting.show');

});

Route::get('editmin', 'UserController@editOrg')->name('editMin');
Route::put('editmin', 'UserController@updateOrg')->name('updateOrg');
Route::get('/moveTo/{id?}','TopicController@moveTo')->name('moveTo');
Route::get('/moveToThisMeeting/{id?}/{subject_id}','TopicController@moveToThisMeeting')->name('moveToThisMeeting');


